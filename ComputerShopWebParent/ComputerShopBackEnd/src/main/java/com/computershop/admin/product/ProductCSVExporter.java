package com.computershop.admin.product;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.computershop.admin.AbstractExporter;
import com.computershop.common.entity.product.Product;

public class ProductCSVExporter extends AbstractExporter{

	public void export(List<Product> listProducts, HttpServletResponse response) throws IOException {
		
		super.setResponseHeader(response, "text/csv", ".csv", "products_");
		
		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), 
				CsvPreference.STANDARD_PREFERENCE);
		
		String[] csvHeader = {"ProductID", "Product Name", "Description", "Product Price", "Category", "Brand"};
		String[] fieldMapping = {"id", "name", "shortDescription", "price", "category", "brand"};
		
		csvWriter.writeHeader(csvHeader);
		
		for (Product product : listProducts) {
			csvWriter.write(product, fieldMapping);
		}
		
		csvWriter.close();
	}

	
}
