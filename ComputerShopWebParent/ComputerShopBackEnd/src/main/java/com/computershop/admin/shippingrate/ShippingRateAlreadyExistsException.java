package com.computershop.admin.shippingrate;

public class ShippingRateAlreadyExistsException extends Exception {

	public ShippingRateAlreadyExistsException(String message) {
		super(message);
	}

}
