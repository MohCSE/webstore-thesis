package com.computershop.admin.setting;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.computershop.common.entity.setting.Setting;
import com.computershop.common.entity.setting.SettingCategory;

public interface SettingRepository extends CrudRepository<Setting, String> {
	public List<Setting> findByCategory(SettingCategory category);
}
