package com.computershop.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan({"com.computershop.common.entity", "com.computershop.admin.user"})
public class ComputerShopBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComputerShopBackEndApplication.class, args);
	}

}
