package com.computershop.admin.report;

public enum ReportType {
	DAY, MONTH, CATEGORY, PRODUCT
}
