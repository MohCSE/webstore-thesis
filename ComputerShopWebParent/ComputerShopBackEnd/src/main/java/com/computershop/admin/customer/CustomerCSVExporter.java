package com.computershop.admin.customer;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.computershop.admin.AbstractExporter;
import com.computershop.common.entity.Customer;

public class CustomerCSVExporter extends AbstractExporter{

	public void export(List<Customer> listCustomers, HttpServletResponse response) throws IOException {
		
		super.setResponseHeader(response, "text/csv", ".csv", "customers_");
		
		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), 
				CsvPreference.STANDARD_PREFERENCE);
		
		String[] csvHeader = {"Customer ID", "E-mail", "Fisrt Name", "Last Name", "Address Line 1", "City", "Country", "Enabled"};
		String[] fieldMapping = {"id", "email", "firstName", "lastName", "addressLine1", "city", "country", "enabled"};
		
		csvWriter.writeHeader(csvHeader);
		
		for (Customer customer : listCustomers) {
			customer.setFirstName(customer.getFirstName());
			customer.setLastName(customer.getLastName());
			customer.setCity(customer.getCity());
			customer.setCountry(customer.getCountry());
			customer.setAddressLine1(customer.getAddressLine1());
			csvWriter.write(customer, fieldMapping);
		}
		
		csvWriter.close();
	}

	
}