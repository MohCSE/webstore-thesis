package com.computershop.shoppingcart;

public class ShoppingCartException extends Exception {

	public ShoppingCartException(String message) {
		super(message);
	}

}
