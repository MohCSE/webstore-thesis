package com.computershop.shipping;

import org.springframework.data.repository.CrudRepository;

import com.computershop.common.entity.Country;
import com.computershop.common.entity.ShippingRate;

public interface ShippingRateRepository extends CrudRepository<ShippingRate, Integer> {
	
	public ShippingRate findByCountryAndState(Country country, String state);
}
