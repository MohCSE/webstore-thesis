package com.computershop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComputerShopFrontEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComputerShopFrontEndApplication.class, args);
	}

}
