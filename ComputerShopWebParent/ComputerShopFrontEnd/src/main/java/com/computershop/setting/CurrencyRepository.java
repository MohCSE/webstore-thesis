package com.computershop.setting;

import org.springframework.data.repository.CrudRepository;

import com.computershop.common.entity.Currency;

public interface CurrencyRepository extends CrudRepository<Currency, Integer> {

}
