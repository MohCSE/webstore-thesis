package com.computershop.setting;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.computershop.common.entity.Country;
import com.computershop.common.entity.State;

public interface StateRepository extends CrudRepository<State, Integer> {
	
	public List<State> findByCountryOrderByNameAsc(Country country);
}
