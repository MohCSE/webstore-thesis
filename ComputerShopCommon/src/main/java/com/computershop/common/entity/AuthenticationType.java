package com.computershop.common.entity;

public enum AuthenticationType {
	DATABASE, GOOGLE, FACEBOOK
}
