package com.computershop.common.entity.order;

public enum PaymentMethod {
	COD, CREDIT_CARD, PAYPAL
}
