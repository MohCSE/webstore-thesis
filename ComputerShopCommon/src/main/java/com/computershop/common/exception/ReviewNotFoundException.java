package com.computershop.common.exception;

public class ReviewNotFoundException extends Exception {

	public ReviewNotFoundException(String message) {
		super(message);
	}	
}
